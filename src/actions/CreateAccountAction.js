export const CREATE_USER = 'CREATE_USER';
export const SET_ALLERGIES = 'SET_ALLERGIES';
export const SET_HOUSEHOLDSIZE = 'SET_HOUSEHOLDSIZE';

export function setCredentials(firstname, lastname, email, password) {
    return { type: CREATE_USER, firstname: firstname, lastname: lastname, email: email, password: password }
}

export function setAllergies(allergies) {
    return { type: SET_ALLERGIES, allergies: allergies }
}

export function setHousehold(householdSize) {
    return { type: SET_HOUSEHOLDSIZE, householdSize: householdSize }
}