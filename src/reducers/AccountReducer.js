import { CREATE_USER, SET_ALLERGIES, SET_HOUSEHOLDSIZE } from '../actions/CreateAccountAction';

const initialState = {
    newAccount: []
}

function accountReducer(state = initialState, action) {
    switch(action.type) {
        case CREATE_USER:
            return {
                newAccount: [
                    {
                        firstName: action.firstname,
                        lastName: action.lastname,
                        email: action.email.toLowerCase(),
                        password: action.password
                    }
                ]
            }
        
        case SET_ALLERGIES:
            return {
                newAccount: [
                    ...state.newAccount,
                    {
                        allergies: action.allergies
                    }
                ]
            }
        
        case SET_HOUSEHOLDSIZE:
            return {
                newAccount: [
                    ...state.newAccount,
                    {
                        houseHoldSize: action.householdSize
                    }
                ]
            }
        
        default:
            return state;
    }
}

export default accountReducer;