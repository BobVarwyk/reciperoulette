import React from 'react';
import logo from '../../img/logoWit.svg';
import Header from '../General/Header';
import '../../Startscreen.css';
import { NavLink } from "react-router-dom";
import Register from './register/Register';
import Login from './login/Login';
import PasswordReset from './passwordReset/PasswordReset';
import Activation from './activation/activation';
import PasswordResetForm from './passwordReset/PasswordResetForm';


class StartScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showScreen: "none"
        }
    }

    componentDidMount() {
        this.checkUrlParameter(this.props.match.url);
    }

    componentWillReceiveProps(props) {
        this.checkUrlParameter(props.match.url);
    }

    checkUrlParameter(URL) {
        if(URL === "/login") {
            this.handleState('showScreen', "login");
        }
        if(URL === "/register") {
            this.handleState('showScreen', "register");
        }
        if(URL === "/password-reset") {
            this.handleState('showScreen', "passwordreset");
        }
        if(this.props.match.params.token != null) {
            this.handleState('showScreen', "activation");
        }
        if(this.props.match.params.resettoken != null) {
            this.handleState('showScreen', "passwordresetform");
        }
    }

    handleState(type, value) {
        this.setState({
            [type]: value
        })
    }

    render() {

        return (
            <div className="not-logged-in">
                <div className="logo"> <img src={logo} /> </div>
                <Header identifier="welcome-index" />
                <div className="skew-bar"> </div>
                <div className="cta-container">
                    <div className="call-to-actions">
                        <h2> Maak kennis met nieuwe recepten! </h2>
                        <span> Vind eenvoudig recepten die bij u en uw huishouden passen. </span>
                        <div className="buttons">
                            <NavLink className="btn activated" to="register"> Registreren </NavLink>
                            <NavLink className="btn" to="login"> Inloggen </NavLink>
                        </div>
                    </div>
                </div>
                <Register showScreen={this.state.showScreen} />
                <Login routeHistory={this.props.history} showScreen={this.state.showScreen} />
                <PasswordReset showScreen={this.state.showScreen} />
                <Activation token={this.props.match.params.token} showScreen={this.state.showScreen} />
                <PasswordResetForm showScreen={this.state.showScreen} token={this.props.match.params.resettoken} />
            </div>
        );
    }
}

export default StartScreen;