import React from 'react';
import { NavLink } from "react-router-dom";
import Credentials from './Credentials';
import Allergies from './Allergies';
import Household from './Household';
import ActivationRequest from './ActivationRequest';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        currentStep:1,
    };
  }

  // Set currentStep one up on click previousButton
  nextStep() {
    let currentStep = this.state.currentStep;
    // Als de huidige stap 1 of 2 is, voeg dan 1 toe bij de volgende Button klik
    currentStep = currentStep >= 3? 4: currentStep +1;
    this.setState({
        currentStep: currentStep
    })
  }

  // Set currentStep one back on click previousButton
  previousStep() {
    let currentStep = this.state.currentStep;
    // Als de huidige stap 2 of 3 is, haal dan 1 eraf bij de volgende Button klik
    currentStep = currentStep <= 1? 1: currentStep - 1;
    this.setState({
        currentStep: currentStep
    })
  }


  render() {

    if(this.props.showScreen !== "register") {
      return null;
    }

    return (
      <div className="page-overlay" id="overlay-xy">
        <div className={'front-display-holder'}>
            <div className="top-bar-ui"> <span> Registreren </span> <NavLink to="/"> <i className="fal fa-times"></i> </NavLink> </div> 
            <div className="form-holder">
                <form onSubmit={this.handleSubmit}>
                   <Credentials actions={{
                                    next: this.nextStep.bind(this),
                                    previous: this.previousStep.bind(this),
                                }} currentStep={this.state.currentStep}/>
                    <Allergies  actions={{
                                    next: this.nextStep.bind(this),
                                    previous: this.previousStep.bind(this),
                                }} currentStep={this.state.currentStep}/>
                   <Household   actions={{
                                    next: this.nextStep.bind(this),
                                    previous: this.previousStep.bind(this),
                                }}currentStep={this.state.currentStep}/>
                   <ActivationRequest currentStep={this.state.currentStep}/>
                </form>
            </div>
        </div>
      </div>
    );
  }
}

export default Register;