import React from 'react';
import Input from '../../General/Input';
import { NavLink } from "react-router-dom";
import Link from '../../General/Link';
import ProgressBar from './ProgressBar';
import AccountStore from '../../../store/AccountStore';
import { setCredentials } from '../../../actions/CreateAccountAction';
import { validateFormFields } from '../../../js/InputValidator';

class Credentials extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordConfirm: '',
      errors: []
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleCredentials = this.handleCredentials.bind(this);
  }

  // Auto save the submitted data in the state of the class.
  handleChange(event) {
    const {name, value} = event.target;
    this.setState({
      [name]: value
    });
  }

  handleCredentials() {
    // GET THE OLD TEXT OF THE BUTTON
    const button = document.getElementById("btn").childNodes[0];
    const buttonValue = button.innerHTML;
    // ACTIVATE LOAD ANIMATION
    button.innerHTML = "";
    button.classList.add("spin");

    //REMOVE THE LOAD UI 
    setTimeout(() => {
      // VALIDATE INPUT FOR CORRECT REGEXES AND VALUES
      const errors = validateFormFields(this.state);
      if(errors.firstName == null && errors.lastName == null && errors.email == null && errors.password == null && errors.passwordConfirm == null) {
        try {
          AccountStore.dispatch(setCredentials(this.state.firstName, this.state.lastName, this.state.email, this.state.password));
          this.props.actions.next();
        } catch(error) {
          console.log(error);
        }
      }
      // SET THE NEW ERRORS IN THE STATE
      this.setState({
        errors : errors
      })
      // REMOVE THE LOAD ANIMATION
      button.classList.remove("spin");
      button.innerHTML = buttonValue;
    }, 3000);
  }

  render() {
    if (this.props.currentStep !== 1) { // Prop: The current step
      return null
    }

    return (
      <div className="space-xy register-md">
          <ProgressBar currentStep={this.props.currentStep} />
          <div className="scrollable">
            <label className="no-margin"> Voornaam </label>
            <Input handlechange={this.handleChange} name="firstName" type="text" value={this.state.firstName} error={this.state.errors.firstName}/>
            <label> Achternaam </label>
            <Input handlechange={this.handleChange} name="lastName" type="text" value={this.state.lastName} error={this.state.errors.lastName}/>
            <label> Email </label>
            <Input handlechange={this.handleChange} name="email" type="email" value={this.state.email}  error={this.state.errors.email}/>
            <label> Wachtwoord </label>
            <Input handlechange={this.handleChange} name="password"  type="password" value={this.state.password} error={this.state.errors.password}/>
            <label> Wachtwoord herhalen </label>
            <Input handlechange={this.handleChange} name="passwordConfirm" type="password" value={this.state.passwordConfirm} error={this.state.errors.passwordConfirm}/>
            <div className="controls">
              <Link id="btn" onclick={this.handleCredentials} text="Volgende stap" />
              <NavLink className="toLogin" to="/login"> Inloggen op uw account </NavLink>
            </div>
          </div>
      </div>
    );
  }
}

  export default Credentials;