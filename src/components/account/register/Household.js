
import React from 'react';
import Link from '../../General/Link';
import ProgressBar from './ProgressBar';
import AccountStore from '../../../store/AccountStore';
import { setHousehold } from '../../../actions/CreateAccountAction';

class Household extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            houseHoldSize: 1
        }
        this.handleHouseHoldSizeChange = this.handleHouseHoldSizeChange.bind(this);
        this.finishRegistration = this.finishRegistration.bind(this);
        this.handleApiCall = this.handleApiCall.bind(this);
    }

    handleHouseHoldSizeChange(event) {
        var type = event.target.id;
        var houseHoldSize = this.state.houseHoldSize;

        if(type === "plus") {
            if(houseHoldSize < 12) {
                var newSize = houseHoldSize + 1;
                this.setState({
                    houseHoldSize : newSize
                })
            }
        } else {
            if(houseHoldSize > 1) {
                var newSize = houseHoldSize - 1;
                this.setState({
                    houseHoldSize : newSize
                })
            }
        }
    }

    finishRegistration() {
        // GET THE OLD TEXT OF THE BUTTON
        const button = document.getElementById("btn").childNodes[0];
        const buttonValue = button.innerHTML;
        // ACTIVATE LOAD ANIMATION
        button.innerHTML = "";
        button.classList.add("spin");

        if(AccountStore.dispatch(setHousehold(this.state.houseHoldSize))) {
            this.handleApiCall().then(Response => {
                setTimeout(() => {
                    if(Response.succes === true) {
                        this.props.actions.next();
                    } else {
                        alert("Error gevonden: " + Response.message);
                    }
                    button.classList.remove("spin");
                    button.innerHTML = buttonValue;
                }, 3000);
                
            });
        } 
    }

    async handleApiCall() {
        let accountBody = JSON.stringify({...AccountStore.getState().newAccount[0], ...AccountStore.getState().newAccount[1], ...AccountStore.getState().newAccount[2]});
        // CALL REGISTER API
        const URL = 'http://localhost:8080/register';
        try {
            const response = await fetch(URL, {
                method: 'POST',
                body : accountBody,
                headers: {
                'Content-Type': 'application/json'
                }
            });
            const json = await response.json();
            return json;
        } catch(error) {
            const errorResponse = {
                succes: "error",
                message: error
            } 
            return errorResponse;
        }
    }
 
    render() {

        if (this.props.currentStep !== 3) { // Prop: The current step
            return null
        }

        return (
            <div className="space-xy">
            <ProgressBar currentStep={this.props.currentStep} />
                <div className="custom-step">
                    <p> Vul hier de grootte in van uw huishouden, deze wordt gebruikt in het bepalen van uw wekelijkse recepten. </p>
                    <div className="household-sizer">
                        <Link onclick={this.handleHouseHoldSizeChange} id="min" class="min" text="-"/>
                            <span className="house-size" id="house-size" data-value={this.state.houseHoldSize}> {this.state.houseHoldSize} </span>
                        <Link onclick={this.handleHouseHoldSizeChange} id="plus" class="plus" text="+"/>
                    </div>
                    <div className="controls">
                        <Link onclick={this.props.actions.previous} class="previous" text="Vorige stap" />
                        <Link id="btn" onclick={this.finishRegistration} text="Verzenden" />
                    </div>
                </div>
            </div>
        );
    }
  }

  export default Household;