import React from 'react';
import Link from '../../General/Link';
import ProgressBar from './ProgressBar';
import AccountStore from '../../../store/AccountStore';
import { setAllergies } from '../../../actions/CreateAccountAction';

class Allergies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            types: ["Dairy", "Peanut", "Soy", "Egg", "Seafood", "Sulfite", "Gluten", "Sesame", "Tree nut", "Grain", "Shellfish", "Wheat"],
            selectedTypes: [],
            notSelected: []
        }
        this.handleSave = this.handleSave.bind(this);
        this.handleAllergies = this.handleAllergies.bind(this);
    }

    handleAllergies(event) {
        const {value} = event.target;
        if(!this.state.selectedTypes.some(item => value === item)) {
            let itemToRemove = this.state.notSelected.indexOf(value);
            this.setState({
                selectedTypes: [...this.state.selectedTypes, value],
                notSelected: this.state.notSelected.filter((_, i) => i !== itemToRemove)
            })
        } else {
            let itemToRemove = this.state.selectedTypes.indexOf(value);
            this.setState({
                selectedTypes: this.state.selectedTypes.filter((_, i) => i !== itemToRemove),
                notSelected: [...this.state.notSelected, value]
            })
        }
    }

    handleSave() {
        const button = document.getElementById("btn").childNodes[0];
        const buttonValue = button.innerHTML;

         // ACTIVATE LOAD ANIMATION
        button.innerHTML = "";
        button.classList.add("spin");
        setTimeout(() => {
            try {
                AccountStore.dispatch(setAllergies(this.state.selectedTypes));
                this.props.actions.next();
            } catch(error) {
                console.log(error);
            }
            // REMOVE THE LOAD ANIMATION
            button.classList.remove("spin");
            button.innerHTML = buttonValue;
        }, 3000);
    }

    render() {
        if (this.props.currentStep !== 2) { // Prop: The current step
            return null
        }

        const checkboxes = [];
        for(var typeId in this.state.types) {
            var type = this.state.types[typeId];
            if(this.state.selectedTypes.some(item => type === item)) {
                checkboxes.push(
                <li key={type}> 
                    <div className="allergy">
                        <input onChange={this.handleAllergies} type="checkbox" id={type} name={type} value={type} checked={true}/> 
                        <label htmlFor={type}> {type} </label> 
                        <span className="checkmark"> <i className="fa fa-check"></i> </span>
                    </div>
                </li>
                )
            } else {
                checkboxes.push(
                    <li key={type}> 
                        <div className="allergy">
                            <input onChange={this.handleAllergies} type="checkbox" id={type} name={type} value={type} checked={false}/> 
                            <label htmlFor={type}> {type} </label> 
                            <span className="checkmark"> <i className="fa fa-check"></i> </span>
                        </div>
                    </li>
                )
            }
        }

        return (
            <div className="space-xy allergies">
            <ProgressBar currentStep={this.props.currentStep} />
                <div className="scrollable">
                    <div className="custom-step">
                        <p> U kunt hier allergieen toevoegen aan uw account. Heeft u geen allergieen? klik dan op volgende stap. </p>
                        <div className="select-grid">
                            <ul>
                                {checkboxes}
                            </ul>
                        </div>
                        <div className="controls">
                            <Link onclick={this.props.actions.previous} class="previous" text="Vorige stap" />
                            <Link id="btn" onclick={this.handleSave} text="Volgende stap" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
  }

  export default Allergies;