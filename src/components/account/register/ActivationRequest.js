
import React from 'react';
import Link from '../../General/Link';
import AccountStore from '../../../store/AccountStore';

class ActivationRequest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.resendEmail = this.resendEmail.bind(this);
        this.resendApiCall = this.resendApiCall.bind(this);
    }

    resendEmail() {
        // GET THE OLD TEXT OF THE BUTTON
        const button = document.getElementById("btn").childNodes[0];
        const buttonValue = button.innerHTML;
        // ACTIVATE LOAD ANIMATION
        button.innerHTML = "";
        button.classList.add("spin");
        const textDisplay =  document.getElementById("lbl");

        this.resendApiCall().then(Response => {
           setTimeout(() => {
                if(Response.succes === true) {
                    button.parentElement.style.backgroundColor = "#3fc380";
                    button.parentElement.style.pointerEvents = "none";
                    button.innerHTML = `<i class="fas fa-check"></i>`;
                    textDisplay.innerHTML = Response.message;
                    textDisplay.style.color = "#3fc380";
                } else {
                    button.parentElement.style.backgroundColor = "#cf000f";
                    button.parentElement.style.pointerEvents = "none";
                    button.innerHTML = `<i class="fas fa-times"></i>`;
                    textDisplay.innerHTML = Response.message;
                    textDisplay.style.color = "#cf000f";
                }
                button.classList.remove("spin");
           }, 3000);
        });
    }

    async resendApiCall() {
        const URL = 'http://localhost:8080/account/activate/resend';
        const body = JSON.stringify({...AccountStore.getState().newAccount[0]});
        // Call the API
        try {
            const response = await fetch(URL, {
            method: 'POST',
            body : body,
            headers: {
                'Content-Type': 'application/json'
            }
            });
            const json = await response.json();
            return json;
        } catch(error) {
            const errorResponse = {
                succes: "error",
                Message: error
            }
            return errorResponse;
        }
    }

    render() {

        if (this.props.currentStep !== 4) { // Prop: The current step
            return null
        }

        return (
            <div className="space-xy">
                <div className="custom-step" id="registration-finished">
                    <div className="orange-dot">
                        <i className="fas fa-mailbox"></i>
                    </div>
                    <h3> Uw registratie is voltooid... maar activeer nog wel even uw account! </h3>
                    <p> Uw account is aangemaakt! voordat u Recipe Roulette kunt gebruiken moet u wel even uw account activeren, dit kan via de activatie link die naar uw email verzonden is!  </p>
                    <div className="controls">
                        <span id="lbl"> Mail niet ontvangen? </span>
                        <Link id="btn" onclick={this.resendEmail} text="Opnieuw verzenden" />
                    </div>
                </div>
            </div>
        );
    }
  }

  export default ActivationRequest;