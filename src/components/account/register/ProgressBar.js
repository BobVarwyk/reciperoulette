import React from 'react';

class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentStep: this.props.currentStep
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            currentStep: props.currentStep
        })
    }

    componentDidMount() {
        const UL = document.getElementById("steps");
        const tags = UL.childNodes;

        if(this.state.currentStep === 4) {
            return null;
        }

        for(var i = 0; i < this.state.currentStep; i++) {
            tags[i].classList.add("active");
        }
    }


    render() {
        return (
            <div className="progress-bar">
                <ul className="steps" id="steps">
                    <li> <i className="fas fa-user"> </i> <span> Persoonsgegevens </span> </li>
                    <li> <i className="fal fa-wheat"></i> <span> Allergieen </span> </li>
                    <li> <i className="fal fa-home-lg-alt"></i> <span> Huishouden </span> </li>
                </ul>
            </div>
        );
    }
  }

  export default ProgressBar;