import React from 'react';
import Axios from 'axios';
import ActivationSuccesful from './activationSuccesful';
import ActivationFailed from './activationFailed';
import { NavLink } from "react-router-dom";

class Activation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        token: this.props.token,
        status: "",
        succes: false
    };
    this.resendEmail = this.resendEmail.bind(this);
  }

  componentDidMount() {
    Axios.put(`http://localhost:8080/account/activate/${this.state.token}`, {})
        .then(response => {
            this.setState({
                status: response.data.message,
                succes: response.data.succes
            })
        })
  }

  resendEmail() {
    // GET THE OLD TEXT OF THE BUTTON
    const button = document.getElementById("btn").childNodes[0];
    const buttonValue = button.innerHTML;
    // ACTIVATE LOAD ANIMATION
    button.innerHTML = "";
    button.classList.add("spin");
    button.parentElement.style.pointerEvents = "none";
    
    Axios.post(`http://localhost:8080/account/activate/resend/${this.state.token}`, {})
    .then(response => {
        setTimeout(() => {
            this.setState({
                status: response.data.message,
                succes: response.data.succes
            })
            button.classList.remove("spin");
            button.innerHTML = buttonValue;
            button.parentElement.style.pointerEvents = "unset";
        }, 3000);
    })
  }

  render() {

    if(this.props.showScreen !== "activation") {
        return null;
    }

    return (
        <div className="page-overlay" id="overlay-xy">
            <div className={'front-display-holder '}>
                <div className="top-bar-ui"> <span> Account activatie </span> <NavLink to="/"> <i className="fal fa-times"></i> </NavLink> </div>
                <ActivationSuccesful message={this.state.status} status={this.state.succes} />
                <ActivationFailed handler={this.resendEmail} message={this.state.status} status={this.state.succes} />
            </div>
        </div>
    );
  }
}

export default Activation;