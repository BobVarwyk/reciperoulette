import React from 'react';
import Link from '../../General/Link';

const ActivationFailed = (props) => {

    if(props.status === true) {
        return null;
    }

    return (
        <div className="cta">
            <h2 id="activation-status"> {props.message} </h2>
            <p className="help-text"> Helaas, we kunnen uw account niet activeren via deze link. Vraag direct een nieuwe activatie link aan via de onderstaande knop. </p>
            <Link id="btn" onclick={props.handler} icon='far fa-paper-plane'> <i class="far fa-paper-plane"></i> Nieuwe link verzenden </Link>
            <span className="index"> link wordt verzonden naar uw mail. </span>
        </div>
    );
}

export default ActivationFailed;