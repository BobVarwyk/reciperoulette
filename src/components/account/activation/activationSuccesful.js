import React from 'react';
import { NavLink } from "react-router-dom";

const ActivationSuccesful = (props) => {

    if(props.status === false) {
        return null;
    }

    return (
        <div className="cta">
            <h2 id="activation-status"> {props.message} </h2>
            <p className="help-text"> Gefeliciteerd, uw account is geactiveerd! U kunt nu gebruik maken van Recipe Roulette door in te loggen. </p>
            <NavLink className="toLogin no-center" to="/login"> Inloggen op uw account </NavLink>
        </div>
    );
}

export default ActivationSuccesful;