import React from 'react';
import Input from '../../General/Input';
import { NavLink } from "react-router-dom";
import Link from '../../General/Link';
import Axios from 'axios';

class PasswordReset extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      errors: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleRequest = this.handleRequest.bind(this);
  }

  handleChange(event) {
    const {name, value} = event.target;
    this.setState({
      [name]: value
    });
  }

  handleRequest() {
    // GET THE OLD TEXT OF THE BUTTON
    const button = document.getElementById("btn").childNodes[0];
    const buttonValue = button.innerHTML;
    // ACTIVATE LOAD ANIMATION
    button.innerHTML = "";
    button.classList.add("spin");
    button.parentElement.style.pointerEvents = "none";

    //Get ERROR output
    const log = document.getElementById("output-response");
    
    Axios.post(`http://localhost:8080/account/password/reset`, {
            email: this.state.email
        }).then(response => {
            setTimeout(() => {
              if(response.data.succes) {
                  log.innerHTML = `<i class="far fa-check"></i>` + response.data.message;
                  log.classList.add("succes");
              } else {
                  log.innerHTML = `<i class="fas fa-exclamation-triangle"></i>` + response.data.message;
                  log.classList.add("error");
              }
              button.classList.remove("spin");
              button.innerHTML = buttonValue;
              button.parentElement.style.pointerEvents = "unset";
            }, 3000);
        })
  }

  render() {

    if(this.props.showScreen !== "passwordreset") {
        return null;
    }

    return (
      <div className="page-overlay reset-pw" id="overlay-xy">
        <div className={'front-display-holder '}>
            <div className="top-bar-ui"> <span> Wachtwoord vergeten </span> <NavLink to="/"> <i className="fal fa-times"></i> </NavLink> </div>
            <div className="form-holder">
                <form onSubmit={this.handleSubmit}>
                    <label> Email </label>
                    <Input handlechange={this.handleChange} icon="fas fa-envelope" name="email" type="email"/>
                    <span id="output-response"> Vul uw email adres in om een herstel link te ontvangen. </span>
                    <div className="controls">
                        <Link id="btn" onclick={this.handleRequest} text="Wachtwoord herstellen" />
                        <NavLink className="toLogin" to="/login"> Inloggen op uw account </NavLink>
                    </div>
                </form>
            </div>
        </div>
      </div>
    );
  }
}

export default PasswordReset;