import React from 'react';
import Input from '../../General/Input';
import { NavLink } from "react-router-dom";
import Link from '../../General/Link';
import Axios from 'axios';

class PasswordResetForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        token: this.props.token,
        password: "",
        passwordConfirm: "",
        status: null,
        output: "Vul uw nieuwe wachtwoord in en klik op wachtwoord wijzigen."
    };
    this.handleChange = this.handleChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleChange(event) {
    const {name, value} = event.target;
    this.setState({
      [name]: value
    });
  }

  handlePasswordChange() {
    // GET THE OLD TEXT OF THE BUTTON
    const button = document.getElementById("btn").childNodes[0];
    const buttonValue = button.innerHTML;
    // ACTIVATE LOAD ANIMATION
    button.innerHTML = "";
    button.classList.add("spin");

    Axios.put(`http://localhost:8080/account/password/reset/${this.state.token}`, {
        oldPassword: this.state.password,
        password: this.state.password,
        passwordConfirm: this.state.passwordConfirm
    }).then(response => {
        setTimeout(() => {
            this.setState({
                status: response.data.succes,
                output: response.data.message
            })
            button.classList.remove("spin");
            button.innerHTML = buttonValue;
        }, 3000);
    })
  }


  render() {

    if(this.props.showScreen !== "passwordresetform") {
        return null;
    }

    return (
      <div className="page-overlay reset-pw" id="overlay-xy">
        <div className={'front-display-holder '}>
            <div className="top-bar-ui"> <span> Wachtwoord wijzigen </span> <NavLink to="/"> <i className="fal fa-times"></i> </NavLink> </div>
            <div className="form-holder">
                <form onSubmit={this.handleSubmit}>
                    <label> Wachtwoord </label>
                    <Input handlechange={this.handleChange} name="password" type="password"/>
                    <label> Wachtwoord herhalen </label>
                    <Input handlechange={this.handleChange} name="passwordConfirm" type="password"/>
                    <span id="output-response" className={this.state.status + ""}> {this.state.output} </span>
                    <div className="controls">
                        <Link id="btn" onclick={this.handlePasswordChange} text="Wachtwoord wijzigen" />
                        <NavLink className="toLogin" to="/login"> Inloggen op uw account </NavLink>
                    </div>
                </form>
            </div>
        </div>
      </div>
    );
  }
}

export default PasswordResetForm;