import React from 'react';
import Input from '../../General/Input';
import { NavLink } from "react-router-dom";
import Link from '../../General/Link';
import Axios from 'axios';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleUserLogin = this.handleUserLogin.bind(this);
    }

    // Auto save the submitted data in the state of the class.
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
        [name]: value
        });
    }

    handleUserLogin() {
        // GET THE OLD TEXT OF THE BUTTON
        const button = document.getElementById("btn").childNodes[0];
        const buttonValue = button.innerHTML;
        // ACTIVATE LOAD ANIMATION
        button.innerHTML = "";
        button.classList.add("spin");

        //Get ERROR output
        const log = document.getElementById("output-response");

        Axios.post(`http://localhost:8080/login`, {
            email: this.state.email,
            password: this.state.password
        }).then(response => {
            setTimeout(() => {
                if(response.data.succes) {
                    window.localStorage.setItem("User", JSON.stringify(response.data.message));
                    window.location.replace("/app");
                } else {
                    log.innerHTML = `<i class="fas fa-exclamation-triangle"></i>` + response.data.message;
                    log.classList.add("error");
                }
                button.classList.remove("spin");
                button.innerHTML = buttonValue;
            }, 3000);
        })
    }


    render() {
        
        if(this.props.showScreen !== "login") {
            return null;
        }
      
        return (
            <div className="page-overlay" id="overlay-xy">
                <div className={'front-display-holder '}>
                    <div className="top-bar-ui"> <span> Inloggen </span> <NavLink to="/"> <i className="fal fa-times"></i> </NavLink> </div>
                    <div className="form-holder">
                        <form onSubmit={this.handleSubmit}>
                            <label> Email </label>
                            <Input handlechange={this.handleChange} icon="fas fa-envelope" name="email" type="email"/>
                            <label> Wachtwoord <NavLink className="pw-forgot" to="/password-reset"> Wachtwoord vergeten? </NavLink> </label>
                            <Input handlechange={this.handleChange} icon="fas fa-key" name="password" type="password"/>
                            <span id="output-response"> Vul alle velden in om in te kunnen loggen. </span>
                            <div className="controls">
                                <Link id="btn" onclick={this.handleUserLogin} text="Inloggen" />
                                <NavLink className="toLogin" to="/register"> Account aanmaken </NavLink>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;