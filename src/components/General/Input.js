import React from 'react';

const Input = (props) => {
    return (
        <div className="outer-container">
            <div className="input-holder">
                <input autoComplete="off" name={props.name} onChange={props.handlechange} min={props.min} step={props.step} max={props.max} defaultValue={props.value} type={props.type} />
            </div>
            <span className="error-dsp" id={props.name+"-error-dsp"}> {props.error} </span>
        </div>
    );
}

export default Input;