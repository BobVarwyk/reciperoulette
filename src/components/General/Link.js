import React from 'react';

const Link = (props) => {

    let linkContent = props.iconAlign === "left" ? <span> <i className={props.icon}></i> {props.text} </span> : <span> {props.text}  <i className={props.icon}></i> </span>; 

    return (
        <a id={props.id} href={props.target} onClick={props.onclick} className={"btn btn-send " + props.class}>
            {linkContent}
        </a>
    );
}

export default Link;