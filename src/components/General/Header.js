import React from 'react';

const Header = (props) => {

    return (
        <div className={"page-ui-index "+ props.identifier}>
            <div className="shadow-box">
                <h1 id="page-title"> {props.title} </h1>
            </div>
        </div>
    );
}

export default Header;