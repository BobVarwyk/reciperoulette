import React from 'react';
import Header from '../General/Header';
import EditCredentials from './Setting Components/EditCredentials';
import EditPassword from './Setting Components/EditPassword';
import DeleteAccount from './Setting Components/DeleteAccount';



class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCompDisplayed: 'general-info'
        }
        this.handleComponentChange = this.handleComponentChange.bind(this);
    }
    
    handleComponentChange(index) {
        if(this.state.currentComponent !== index) {
            this.setState({
                currentCompDisplayed: index
            })
            var currentSelected = document.getElementsByClassName('selected');
            currentSelected[0].className = currentSelected[0].className.replace("selected", "");
            document.getElementById(index).className += "selected";
        } 
    }


    render() {

        if(this.props.showScreen !== "settings") {
            return null;
        }

        return (
            <div className="component-container" id="settings">
               <Header title="Uw instellingen" />
               <div className="content-ui-menu">
                    <ul>
                        <li onClick={()=>this.handleComponentChange("general-info")} id="general-info" className="selected"> Algemene gegevens </li>
                        <li onClick={()=>this.handleComponentChange("password-reset")} id="password-reset" className=""> Wachtwoord instellen </li>
                        <li onClick={()=>this.handleComponentChange("account-delete")} id="account-delete" className=""> Account verwijderen </li>
                    </ul>
                </div>
                <div className="center">
                    <EditCredentials currentComponent={this.state.currentCompDisplayed} />
                    <EditPassword currentComponent={this.state.currentCompDisplayed}/>
                    <DeleteAccount currentComponent={this.state.currentCompDisplayed}/>
                </div>
            </div>
        );
    }
}

export default Settings;