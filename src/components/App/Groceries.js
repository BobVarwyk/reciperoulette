import React from 'react';
import Header from '../General/Header';
import GroceryCard from './Grocery Components/GroceryCard';
import FilterMenu from './FilterMenu';
import { getCurrentDayName } from '../../js/Main';
import { AnimateOnChange } from 'react-animation';
import Axios from 'axios';

class Groceries extends React.Component {
    constructor(props) {
        super(props);
        const user = JSON.parse(window.localStorage.getItem("User"));
        this.state = {
            userId: user.id,
            activeDay: getCurrentDayName(),
            groceries: [],
            activeGroceries: []
        }
    }

    componentDidMount() {
        Axios.get(`http://localhost:8080/groceries/${this.state.userId}/get/all`)
        .then(response => {
            if(response.data.succes) {
                this.setState({
                    groceries: response.data.message,
                })
            } else {
                console.log(response.data.message);
            }
        }).then(response => {
            this.changeActiveGroceries(this.state.groceries);
        })
    }

    changeDay(day) {
        // Set activeDay to new value given by day parameter in function
        if(this.state.activeDay !== day) {
            this.setState({
                activeDay: day
            }, () => {
                this.changeActiveGroceries(this.state.groceries);
            })
        }
    }

    changeActiveGroceries(groceries) {
        for(var dayKey in groceries) {
            // Get the day selected
            if(dayKey === this.state.activeDay) {
                var selectedGroceries = groceries[dayKey];
                this.setState({
                    activeGroceries: selectedGroceries,
                })
            }
        }
    }
    
    render() {
        if(this.props.showScreen !== "groceries") {
            return null;
        }
        
        return (
            <div className="component-container" id="groceries">
               <Header title="Uw boodschappen deze week." />
               <FilterMenu active={this.state.activeDay} actions={{handleComponentChange : this.changeDay.bind(this)}}/>
               <div className="center"> 
                    <p> Op deze pagina kunt u uw boodschappen vinden voor deze week, u kunt op de recepten pagina ingredienten toevoegen aan uw boodschappenlijst, net zoals de recepten zijn deze per dag ingedeeld. In het bovenstaande menu kunt u van dag veranderen. </p>
                    <ul> 
                        <AnimateOnChange>
                            {this.state.activeGroceries.length > 0 ? (
                                <GroceryCard handler={this.handleGroceryChange} day={this.state.activeDay} groceries={this.state.activeGroceries} /> 
                            ) : (
                                <h2> Er zijn geen boodschappen actief voor deze dag </h2>
                            )}
                        </AnimateOnChange>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Groceries;