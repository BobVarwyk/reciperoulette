import React from 'react';
import MenuBar from './MenuBar';
import AllergiesComponent from './Allergies';
import Diets from './Diets';
import Settings from './Settings';
import './App.css';
import Recipes from './Recipe Components/Recipe';
import Groceries from './Groceries';

class AppInterface extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentComponent: "recipes",
        };
    }

    componentDidMount() {
        this.checkUrlParameter(this.props.match.params.component);
    }

    componentWillReceiveProps(props) {
        this.checkUrlParameter(props.match.params.component);
    }

    checkUrlParameter(URL) {
        if(URL === "allergies") {
            this.handleState('currentComponent', "allergies");
        }
        if(URL === "diets") {
            this.handleState('currentComponent', "diets");
        }
        if(URL === "settings") {
            this.handleState('currentComponent', "settings");
        }
        if(URL === "recipes" || URL === "app") {
            this.handleState('currentComponent', "recipes");
        }
        if(URL === "groceries") {
            this.handleState('currentComponent', "groceries");
        }
    }

    handleState(type, value) {
        this.setState({
            [type]: value
        })
    }

    render() {
        return (
            <div className="app-interface">
                <MenuBar history={this.props.history} menuHandler={this.toggleMenu} componentHandler={this.handleComponentChange}/>
                <div className="content-ui-box">
                    <Recipes showScreen={this.state.currentComponent} componentchange={this.toggleMenu}/>
                    <AllergiesComponent showScreen={this.state.currentComponent} />
                    <Diets showScreen={this.state.currentComponent} />
                    <Groceries showScreen={this.state.currentComponent} />
                    <Settings showScreen={this.state.currentComponent}/>
                </div>
            </div>
        );
    }
}

export default AppInterface;