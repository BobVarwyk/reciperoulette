import React from 'react';
import { NavLink } from "react-router-dom";

const RecipeCardSkeleton = (props) => {
    return (
        <div className="recipe-card" id="skeleton">
            <div className="img-container">
                <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div>
            </div>
            <div className="recipe-data">
                <div className="content-spacer">
                    <h2 className="recipe-title"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </h2>
                    <ul>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                    </ul>
                    <NavLink className="btn" to="/"> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </NavLink>
                </div>
            </div>

            <div className="additional-data">
                <div className="left-view">
                    <h3> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </h3>
                    <ul>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                        <li> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </li>
                    </ul>
                </div>
                <div className="right-view">
                    <h3> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </h3>
                    <table> 
                        <tbody>
                            <tr>
                                <th> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </th>
                                <th> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </th>
                            </tr>   
                            <tr> 
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div></td>
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div>  </td>
                            </tr>  
                            <tr> 
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </td>
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </td>
                            </tr>    
                            <tr> 
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </td>
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </td>
                            </tr>    
                            <tr> 
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </td>
                                <td> <div className="skeleton-loader"> <div className="skeleton-line"> </div> </div> </td>
                            </tr>      
                        </tbody>         
                    </table>
                </div>
            </div>
        </div>
    );
}

export default RecipeCardSkeleton;