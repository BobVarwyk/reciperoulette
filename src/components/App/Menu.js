import React from 'react';
import { NavLink } from "react-router-dom";

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.destroySession = this.destroySession.bind(this);
    }

    destroySession() {
        window.localStorage.clear();
        window.location.href = "/";
    }

    render() {
        return (
            <div className="menu hidden" id="menu">
                <div className="top-bar" id="top-bar-ui">
                    <NavLink onClick={this.closeMenu} to="/recipes">  <i className="fas fa-home-lg-alt"></i> </NavLink>
                </div>

                <div className="menu-bar">
                    <NavLink onClick={this.props.menuHandler} to="/recipes"> <i className="fas fa-utensils"></i> <span> recepten </span></NavLink>
                    <NavLink onClick={this.props.menuHandler} to="/allergies"> <i className="fal fa-wheat"></i> <span> allergiën </span> </NavLink>
                    <NavLink onClick={this.props.menuHandler} to="/diets"> <i className="fas fa-french-fries"></i> <span> Diëten </span> </NavLink>
                    <NavLink onClick={this.props.menuHandler} to="/groceries"> <i className="fas fa-sticky-note"></i> <span> Boodschappen </span></NavLink>
                    <NavLink onClick={this.props.menuHandler} to="/settings"> <i className="fas fa-cog"></i> <span> Instellingen </span> </NavLink>
                    <button id="logout" onClick={this.destroySession}> <i className="fal fa-sign-out-alt"></i> <span> Uitloggen </span> </button>
                </div>
            </div> 
        )
    }
}

export default Menu;