import React from 'react';


class FilterMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        // Set the new option to active
        let currentActiveOption = document.getElementById(this.props.active)
        currentActiveOption.classList.add("active");
    }

    componentDidUpdate() {
        // Get the current active element and remove the active class
        let currentActiveElement = document.getElementsByClassName("active")[0]
        currentActiveElement.classList.remove("active")

        // Set the new option to active
        let currentActiveOption = document.getElementById(this.props.active)
        currentActiveOption.classList.add("active");
    }

    shouldComponentUpdate(nextProps, prevProps) {
        if(nextProps.active !== prevProps.active) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <div className="content-ui-menu">
                <ul>
                    <li onClick={()=>this.props.actions.handleComponentChange("maandag")} id="maandag"> Maandag </li>
                    <li onClick={()=>this.props.actions.handleComponentChange("dinsdag")} id="dinsdag"> Dinsdag </li>
                    <li onClick={()=>this.props.actions.handleComponentChange("woensdag")} id="woensdag"> Woensdag </li>
                    <li onClick={()=>this.props.actions.handleComponentChange("donderdag")} id="donderdag"> Donderdag </li>
                    <li onClick={()=>this.props.actions.handleComponentChange("vrijdag")} id="vrijdag"> Vrijdag </li>
                    <li onClick={()=>this.props.actions.handleComponentChange("zaterdag")} id="zaterdag"> Zaterdag </li>
                    <li onClick={()=>this.props.actions.handleComponentChange("zondag")} id="zondag"> Zondag </li>
                </ul>
            </div>
        );
    }
}

export default FilterMenu;