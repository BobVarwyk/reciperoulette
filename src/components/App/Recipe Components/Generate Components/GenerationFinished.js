import React from 'react';
import { NavLink } from "react-router-dom";

const GenerationFinished = (props) => {
    return (
        <div className="content">
            <h1> We hebben 7 nieuwe recepten voor je uitgekozen, {props.user}! </h1>
            <span> Er zijn 7 nieuwe recepten voor de komende week uitgekozen voor je, dit is gedaan op basis van jouw persoonlijk profiel. Veel kook & eetplezier! </span> 
            <div className="recipe-display">
                <ul className="recipe-illustrations">
                    <li> <i className="fas fa-french-fries"></i> </li>
                    <li> <i className="fas fa-soup"></i> </li>
                    <li> <i className="far fa-steak"></i> </li>
                    <li> <i className="fas fa-turkey"></i> </li>
                    <li> <i className="fas fa-fish-cooked"></i> </li>
                    <li> <i className="fas fa-egg-fried"></i> </li>
                    <li> <i className="fas fa-pizza-slice"></i> </li>
                </ul>
                <NavLink className="btn" to="/"> Bekijk snel al uw gekozen recepten </NavLink>
            </div>
        </div>
    );
}

export default GenerationFinished;