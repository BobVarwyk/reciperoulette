import React from 'react';
import {ReactComponent as RouletteWheel} from '../../../../img/roulette.svg';

const RouletteWheelView = (props) => {
    return (
        <div className="content">
            <h1> Hoi {props.user}, het is tijd om nieuwe recepten te kiezen! </h1>
            <span> Let op! De recepten zijn voor de komende 7 dagen actief, u kunt in die tijd geen nieuwe recepten aanvragen. Zorg ervoor dat uw allergiën en diet juist zijn ingesteld. </span> 
            <div className="roulette-wheel">
                <RouletteWheel />
                <div className="roulette-trigger" onClick={props.handler}>
                    <h2> Klik hier! </h2>
                </div>
            </div>
        </div>
    );
}

export default RouletteWheelView;