import React from 'react';
import Axios from 'axios';
import RouletteWheelView from './RouletteWheel';
import { AnimateOnChange } from 'react-animation';
import GenerationFinished from './GenerationFinished';
import { NavLink } from "react-router-dom";

class GenerateSchedule extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: JSON.parse(localStorage.getItem("User")).id,
            userName: JSON.parse(localStorage.getItem("User")).firstName,
            generateSuccesful: false
        }
        this.rotateWheel = this.rotateWheel.bind(this);
        this.handleGenerateCall = this.handleGenerateCall.bind(this);
    }

    rotateWheel(activate) {
        // Get the roulette Wheel SVG
        var rouletteWheel = document.getElementsByClassName("roulette-svg")[0];
        // Add or remove the matching CSS class for the animation.
        if(activate === true) {
            rouletteWheel.classList.add("spin-animate");
        } else {
            rouletteWheel.classList.remove("spin-animate");
        }
    }

    handleGenerateCall() {
        this.rotateWheel(true)
        Axios.get(`http://localhost:8080/recipes/generate/${this.state.userId}`)
        .then(response => {
            if(response.data.succes == true) {
               console.log(response.data.message)
               setTimeout(() => {
                this.rotateWheel(false);
                this.setState({
                    generateSuccesful: true
                })
            }, 3500);
            } else {
                console.log("ERROR : ", response.data.message)
            }
        });
    }
   
    render() {
        return (
            <div className="component-container" id="generate">
                <div className="generate-view">
                    <div className="close-generation-screen"> <NavLink to="/"> <i className="far fa-times"></i> </NavLink> </div>
                    <div className="centered"> 
                        <AnimateOnChange>     
                            {this.state.generateSuccesful ? (
                                <GenerationFinished user={this.state.userName} />
                            ) : (
                                <RouletteWheelView user={this.state.userName} handler={this.handleGenerateCall} />
                            )}
                        </AnimateOnChange>      
                    </div>
                </div>
            </div>
        );
    }
}

export default GenerateSchedule;