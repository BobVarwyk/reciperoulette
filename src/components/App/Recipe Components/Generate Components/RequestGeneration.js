import React from 'react';
import { NavLink } from "react-router-dom";

const RequestGeneration = (props) => {
    return (
        <div className="request-generation">
            <div className="icon"> <i className="far fa-grin-beam-sweat"></i> </div>
            <h2> Het ziet er naar uit dat je nog geen recepten hebt voor deze week! </h2>
            <NavLink className="btn" to="/generate/recipes"> Vraag snel nieuwe recepten aan! </NavLink>
        </div>
    );
}

export default RequestGeneration;