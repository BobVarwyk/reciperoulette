import React from 'react';
import Header from '../../General/Header';
import FilterMenu from '../FilterMenu';
import RecipeCard from './RecipeCard';
import { AnimateOnChange } from 'react-animation';
import RecipeCardSkeleton from '../Load Components/RecipeCardSkeleton';
import { getCurrentDayName } from '../../../js/Main';
import Axios from 'axios';
import RequestGeneration from './Generate Components/RequestGeneration';

class Recipes extends React.Component {
    constructor(props) {
        super(props);
        const user = JSON.parse(window.localStorage.getItem("User"))

        this.state = {
            userId: user.id,
            activeDay: getCurrentDayName(),
            recipes: [],
            activeRecipe: {},
            active: false,
            loading: true,
            errors: []
        }
    }

    componentDidMount() { 
       if(localStorage.getItem("recipes") !== null) {
            this.getSchedule();
       } else {
            this.requestRecipeInfoFromApi();
       }
    }

    // Get the recipe data from the REST API if localstorage isn't set.
    async requestRecipeInfoFromApi() {
        Axios.get(`http://localhost:8080/${this.state.userId}/recipe/get/all`)
        .then(response => {
            if(response.data.succes) {
                if(this.isScheduleExpired(response.data.expireDate)) {
                    this.changeExpiredState(true);
                    return null;
                }
                console.log(response.data.message)
                // Set the new state based on the result of API call
                this.handleRecipeStateChange(response.data.message, false)
                // Set the localstorage with the same information to reduce API calls
                this.handleLocalStorageRecipeChange(response.data.message, response.data.expireDate)
            } else {
                // Set the error display
                this.changeState("errors", response.data.message)
                this.changeExpiredState(true)
            }
        }).then(response => {
            this.changeRecipe(this.state.recipes);
        })
    }

    getSchedule() {
        if(localStorage.getItem("recipes") !== null) {
            if(this.isScheduleExpired(JSON.parse(localStorage.getItem("expireDate")))) {
                this.changeExpiredState(true);
                return null;
            } 
            this.setState({
                recipes: JSON.parse(localStorage.getItem("recipes")),
                scheduleExpireDate: JSON.parse(localStorage.getItem("expireDate")),
                loading: false
            },
                this.changeRecipe(JSON.parse(localStorage.getItem("recipes")))
            )
        }
    }

    handleRecipeStateChange(recipesObject, loadingStatus) {
        this.setState({
            recipes: recipesObject,
            loading: loadingStatus
        })
    }

    handleLocalStorageRecipeChange(recipes, expirationdate) {
        localStorage.setItem("recipes", JSON.stringify(recipes))
        localStorage.setItem("expireDate", JSON.stringify(expirationdate))
    }

    changeRecipe(schedule) {
        schedule.forEach((recipe) => {
            if(recipe.day === this.state.activeDay) {
                this.setState({activeRecipe : recipe})
            }
        })
    }

    changeDay(day) {
        // Set activeDay to new value given by day parameter in function
        if(this.state.activeDay !== day) {
            this.setState({
                activeDay: day
            }, () => {
                this.changeRecipe(this.state.recipes);
            })
        }
    }

    changeState(key, value) {
        this.setState({
            [key] : value
        })
    }

    changeExpiredState(status) {
        this.setState({
            active: status,
            loading: false
        })
    }

    // Check if schedule is still valid
    isScheduleExpired(dateFromJson) {
        // Convert state expireDate to Javascript data
        let expirationDate = new Date(dateFromJson)
        // Get the currentDate
        let currentDate = new Date()
        let returnValue = null
        if(currentDate > expirationDate) {
            returnValue = true
        } else {
            returnValue = false
        }
        return returnValue
    }

    render() {
        if(this.props.showScreen !== "recipes") {
            return null;
        } 

        return (
            <div className="component-container" id="recipes">
               <Header title="Uw recepten deze week" />
               <FilterMenu active={this.state.activeDay} actions={{handleComponentChange : this.changeDay.bind(this)}}/>
               <div className="center">
                    <AnimateOnChange>
                        {this.state.active ? (
                            <RequestGeneration />
                        ) : (
                            this.state.loading ? (
                                <RecipeCardSkeleton />
                            ) : (
                                <RecipeCard recipe={this.state.activeRecipe} />
                            )
                        )}
                    </AnimateOnChange>
               </div>
            </div>
        );
    }
}

export default Recipes;