import React from 'react';
import { NavLink } from "react-router-dom";

class RecipeCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.recipe.id,
            title: this.props.recipe.title,
            dishType: this.props.recipe.dishType,
            servingTime: this.props.recipe.servingTime,
            servings: this.props.recipe.servings,
            ingredients: this.props.recipe.ingredients,
            nutrients: this.setNutrients()
        }
    }

    componentDidUpdate() {
        this.setState({
            id: this.props.recipe.id,
            title: this.props.recipe.title,
            dishType: this.props.recipe.dishType,
            servingTime: this.props.recipe.servingTime,
            servings: this.props.recipe.servings,
            ingredients: this.props.recipe.ingredients,
            nutrients: this.setNutrients()
        })
    }

    shouldComponentUpdate(nextProps) {
        if(nextProps.recipe.title !== this.state.title) {
            return true;
        }
        return false;
    }

    setNutrients() {
        let nutrients = {
            Calories: "",
            Carbohydrates: "",
            Protein: "",
            Fat: ""
        }
        // Loop through all nutrients and select the values needed in the switch statement. 
        this.props.recipe.nutrients.forEach((nutrient) => {
            let amountOfNutrient = Math.round(nutrient.amount);
            switch(nutrient.title) {
                case "Calories":
                    nutrients.Calories = amountOfNutrient;
                break;
                case "Carbohydrates":
                    nutrients.Carbohydrates = amountOfNutrient;
                break;
                case "Protein":
                    nutrients.Protein = amountOfNutrient;
                break;
                case "Fat":
                    nutrients.Fat = amountOfNutrient;
                break;
            }
        })
        return nutrients
    }

    render() {
        
        let ingredientsList = []
        this.state.ingredients.slice(0, 5).forEach((ingredient) => {
            ingredientsList.push(
                <li key={ingredient.id + Math.random(0, 1000)}> <span> {ingredient.amount} {ingredient.unit} {ingredient.name} </span> </li>
            )
        });

        return (
            <div className="recipe-card">
                <div className="img-container">
                    <img src={this.props.recipe.image} />
                    <div className="date"> <span> {this.props.day} </span> </div>
                </div>
                <div className="recipe-data">
                    <div className="content-spacer">
                        <h2 className="recipe-title"> {this.state.title} </h2>
                        <ul>
                            <li> <i className="fal fa-utensils"></i> Hoofdgerecht </li>
                            <li> <i className="far fa-clock"></i> {this.state.servingTime} minuten </li>
                            <li> <i className="fas fa-users"></i> {this.state.servings} personen </li>
                        </ul>
                        <NavLink className="btn" to={"/recipe/" + this.state.id}> Bekijk recept </NavLink>
                    </div>
                </div>
    
                <div className="additional-data">
                    <div className="left-view">
                        <h3> Ingredienten </h3>
                        <ul>
                            {ingredientsList}
                            <li> * Bekijk alle ingredienten op de recepten pagina</li>
                        </ul>
                    </div>
                    <div className="right-view">
                        <h3> Voedingswaarden </h3>
                        <table> 
                            <tbody>
                                <tr>
                                    <th>  </th>
                                    <th> Per portie </th>
                                </tr>   
                                <tr> 
                                    <td> Energie (Kcal) </td>
                                    <td> {this.state.nutrients.Calories} </td>
                                </tr>  
                                <tr> 
                                    <td> Koolhydraten (Gram) </td>
                                    <td> {this.state.nutrients.Carbohydrates} </td>
                                </tr>    
                                <tr> 
                                    <td> Eiwitten (Gram) </td>
                                    <td> {this.state.nutrients.Protein} </td>
                                </tr>    
                                <tr> 
                                    <td> Vet (Gram) </td>
                                    <td> {this.state.nutrients.Fat}  </td>
                                </tr>      
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default RecipeCard;