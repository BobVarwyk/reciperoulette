import React from 'react';
import Header from '../General/Header';
import Axios from 'axios';


class Diets extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        const user = JSON.parse(window.localStorage.getItem("User"));
        this.state = {
            userId: user.id,
            types: ["Gluten", "Ketogenic", "Vegetarian", "Lacto-Vegetarian", "Ovo-Vegetarian", "Vegan", "Pescetarian", "Paleo", "Primal", "Whole30"],
            selectedTypes : [],
            notSelected: ["Gluten", "Ketogenic", "Vegetarian", "Lacto-Vegetarian", "Ovo-Vegetarian", "Vegan", "Pescetarian", "Paleo", "Primal", "Whole30"]
        }
        this.handleChange = this.handleChange.bind(this);
    }
    
    componentDidMount() {
        this._isMounted = true;
        Axios.get(`http://localhost:8080/diets/${this.state.userId}`)
            .then(response => {
                const selected = response.data.message;
                const notSelected = this.state.notSelected.filter((element) => !selected.includes(element));
                if(selected != null) {
                    this.setState({
                        selectedTypes: selected,
                        notSelected: notSelected
                    })
                }
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleChange(event) {
        const { value } = event.target;
        Axios.put(`http://localhost:8080/account/diets/${this.state.userId}`, {
            dietName: value
        }).then(response => {
            const selected = response.data.message;
            const notSelected = this.state.notSelected.filter((element) => !selected.includes(element));
            this.setState({
                selectedTypes: selected,
                notSelected: notSelected
            })
        })
    }
    
    render() {
        if(this.props.showScreen !== "diets") {
            return null;
        }
        
        
         // Render all the checkboxes, based on whether they are in the selected array or non selected array
         const Toggles = [];
         for(var typeId in this.state.types) {
             var type = this.state.types[typeId];
             if(this.state.selectedTypes.some(item => type === item)) {
                 Toggles.push(
                    <li key={type}> 
                        {type}
                        <label key={type} className="switch">
                            <input onChange={this.handleChange} value={type} type="checkbox" checked={true}/>
                            <span className="slider round"></span>
                        </label>
                    </li>
                 );
             } else {
                 Toggles.push(
                    <li key={type}> 
                        {type}
                        <label key={type} className="switch">
                            <input onChange={this.handleChange} value={type} type="checkbox" checked={false}/>
                            <span className="slider round"></span>
                        </label>
                    </li>
                 );
             }
         }
        return (
            <div className="component-container" id="allergies">
               <Header title="Uw dieten" />
               <div className="center"> 
                    <p> Op deze pagina kunt u dieten op actief en inactief zetten op uw account. Deze allergieen zullen in de eerst volgende week recepten worden meegenomen en hier zal rekening mee worden gehouden. </p>
                    <ul> 
                        {Toggles}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Diets;