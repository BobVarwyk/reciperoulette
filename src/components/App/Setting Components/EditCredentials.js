import React from 'react';
import Input from '../../General/Input';
import Link from '../../General/Link';
import Axios from 'axios';

class EditCredentials extends React.Component {
    constructor(props) {
        super(props);
        const user = JSON.parse(window.localStorage.getItem("User"));
        this.state = {
            userId: user.id,
            firstName: '',
            lastName: '',
            email: '',
            householdSize: '',
            status: null,
            outputMsg: 'Let op! Uw oude gegevens gaan verloren als u uw account wijzigt.'  
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    componentDidMount() {
        const URL = `http://localhost:8080/account/${this.state.userId}`;
        fetch(URL).then(response => response.json())
        .then((result) => {
           this.setState({
               firstName: result.firstName,
               lastName: result.lastName,
               email: result.email,
               householdSize: result.householdSize
           })
        });
    }

    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }

    handleUpdate() {
        // GET THE OLD TEXT OF THE BUTTON
        const button = document.getElementById("btn").childNodes[0];
        const buttonValue = button.innerHTML;
        // ACTIVATE LOAD ANIMATION
        button.innerHTML = "";
        button.classList.add("spin");

        Axios.put(`http://localhost:8080/account/update/${this.state.userId}`, {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            houseHoldSize: this.state.householdSize
        }).then(response => {
            setTimeout(() => {
                this.setState({
                    status: response.data.succes,
                    outputMsg: response.data.message
                })
                button.classList.remove("spin");
                button.innerHTML = buttonValue;
            }, 3000);
        })
    }

    render() {

        // Do not show component if current component of parent state isn't general info
        if(this.props.currentComponent !== "general-info") {
            return null;
        }

        return (
            <div className="settings-holder">
                <p> Op deze pagina kunt u uw algemene account gegegevens wijzigen, Wilt u uw account wachtwoord aanpassen of uw account verwijderen? Klik dan in het menu hierboven op de bijbehorende optie.</p>
                <form onSubmit={this.handleSubmit}>
                    <label> Voornaam </label>
                    <Input handlechange={this.handleChange} icon="fas fa-user" name="firstName" placeholder="Voornaam.." type="text" value={this.state.firstName}/>
                    <label> Achternaam </label>
                    <Input handlechange={this.handleChange} icon="fas fa-user" name="lastName" placeholder="Achternaam.." type="text" value={this.state.lastName}/>
                    <label> Email adres </label>
                    <Input handlechange={this.handleChange} icon="fas fa-envelope" name="email" placeholder="email.." type="email" value={this.state.email}/>
                    <label> Aantal personen in huishouden </label>
                    <Input handlechange={this.handleChange} icon="fas fa-users" name="householdSize" placeholder="Aantal personen huishouden.." step="1" min="1" max="12" type="number" value={this.state.householdSize}/>
                    <span id="output-response" className={this.state.status + ""}> {this.state.outputMsg} </span>
                    <Link id="btn" onclick={this.handleUpdate} text="Wijzig account"/> 
                </form>
            </div>
        )
    }
}

export default EditCredentials;