import React from 'react';
import Input from '../../General/Input';
import Link from '../../General/Link';
import Axios from 'axios';

class EditPassword extends React.Component {
    constructor(props) {
        super(props);
        const user = JSON.parse(window.localStorage.getItem("User"));
        this.state = {
            userId: user.id,
            previousPassword: "",
            password: "",
            passwordConfirm: "",
            status: null,
            outputMsg: "U kunt hier een nieuw wachtwoord instellen voor uw account."
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }

    handleUpdate() {
        // GET THE OLD TEXT OF THE BUTTON
        const button = document.getElementById("btn").childNodes[0];
        const buttonValue = button.innerHTML;
        // ACTIVATE LOAD ANIMATION
        button.innerHTML = "";
        button.classList.add("spin");

        Axios.put(`http://localhost:8080/account/update/password/${this.state.userId}`, {
            oldPassword: this.state.previousPassword,
            password: this.state.password,
            passwordConfirm: this.state.passwordConfirm
        }).then(response => {
            setTimeout(() => {
                this.setState({
                    status: response.data.succes,
                    outputMsg: response.data.message
                })
                button.classList.remove("spin");
                button.innerHTML = buttonValue;
            }, 3000);
        })
    }

    render() {

        if(this.props.currentComponent !== "password-reset") {
            return null;
        }

        return (
            <div className="settings-holder">
                <p> Op deze pagina kunt u uw wachtwoord wijzigen, Wilt u uw account gegevens aanpassen of uw account verwijderen? Klik dan in het menu hierboven op de bijbehorende optie.</p>
                <form onSubmit={this.handleSubmit}>
                    <label> Uw oude wachtwoord </label>
                    <Input handlechange={this.handleChange} icon="fas fa-key" name="previousPassword" placeholder="Oud wachtwoord.." type="password"/>
                    <label> Nieuw wachtwoord </label>
                    <Input handlechange={this.handleChange} icon="fas fa-key" name="password" placeholder="Nieuw wachtwoord.." type="password"/>
                    <label> Nieuw wachtwoord herhalen </label>
                    <Input handlechange={this.handleChange} icon="fas fa-key" name="passwordConfirm" placeholder="Nieuw wachtwoord herhalen.." type="password"/>
                    <span id="output-response" className={this.state.status + ""}> {this.state.outputMsg} </span>
                    <Link id="btn" onclick={this.handleUpdate} text="Wachtwoord wijzigen"/>
                </form>
            </div>
        )
    }
}

export default EditPassword;