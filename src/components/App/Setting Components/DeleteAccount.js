import React from 'react';
import Input from '../../General/Input';
import Link from '../../General/Link';
import Axios from 'axios';

class DeleteAccount extends React.Component {
    constructor(props) {
        super(props);
        const user = JSON.parse(window.localStorage.getItem("User"));
        this.state = {
            userId: user.id,
            password: "",
            status: null,
            outputMsg: "Uw account zal definitief worden verwijderd"
        }
        this.handleChange = this.handleChange.bind(this);
        this.openConfirm = this.openConfirm.bind(this);
        this.closeConfirm = this.closeConfirm.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    openConfirm() {
        var confirmBox = document.getElementById('delete-submit');
        confirmBox.style.display = "flex";
        confirmBox.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }

    closeConfirm() {
        var confirmBox = document.getElementById('delete-submit');
        confirmBox.style.display = "none";
    }

    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }

    handleDelete() {
        // GET THE OLD TEXT OF THE BUTTON
        const button = document.getElementById("btn").childNodes[0];
        const buttonValue = button.innerHTML;
        // ACTIVATE LOAD ANIMATION
        button.innerHTML = "";
        button.classList.add("spin");

        Axios.delete(`http://localhost:8080/account/delete/${this.state.userId}`, {
            data: {password: this.state.password}
        }).then(response => {
            setTimeout(() => {
                this.setState({
                    status: response.data.succes,
                    outputMsg: response.data.message
                })
                button.classList.remove("spin");
                button.innerHTML = buttonValue;
                if(response.data.succes === true) {
                    setTimeout(() => {
                        window.localStorage.clear();
                        window.location.reload();
                    }, 5000);
                }
            }, 3000);
        })
    }

    render() {

        if(this.props.currentComponent !== "account-delete") {
            return null;
        }

        return (
            <div className="settings-holder" id="del-account">
                <p> Op deze pagina kunt u uw account verwijderen, Wilt u uw account gegevens aanpassen of wachtwoord aanpassen? Klik dan in het menu hierboven op de bijbehorende optie.</p>
                <Link onclick={this.openConfirm} text="Account verwijderen"/>

                <div className="delete-account-submit" id="delete-submit"> 
                    <div className="inner">
                        <div className="top-bar"> <h3> Weet u het zeker? </h3> <i onClick={this.closeConfirm} className="far fa-times"></i> </div>
                        <p className="extra-info"> Voer uw wachtwoord in om uw account definitief te verwijderen. <strong> Let op! </strong> Als u uw account verwijdert is er geen kans meer om deze terug te krijgen. </p>
                        <form onSubmit={this.handleSubmit}>
                            <label> Voer uw wachtwoord in </label>
                            <Input handlechange={this.handleChange} icon="fas fa-key" name="password" type="password"/>
                            <span id="output-response" className={this.state.status + ""}> {this.state.outputMsg} </span>
                            <Link onclick={this.handleDelete} id="btn" text="Verwijder account"/>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default DeleteAccount;