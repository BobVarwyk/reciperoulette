import React from 'react';
import Menu from './Menu';

class MenuBar extends React.Component {
    constructor(props) {
        super(props);
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    toggleMenu() {
        var menu = document.getElementById("menu");
        var overlay = document.getElementById("overlay");
        if(menu.classList.contains("hidden")) {
            menu.classList.remove("hidden");
            menu.classList.add("visible");
            overlay.style.visibility = "visible";
        } else {
            menu.classList.remove("visible");
            menu.classList.add("hidden");
            overlay.style.visibility = "hidden";
        }
    }

    render() {
        const accountDetails = JSON.parse(localStorage.getItem("User"));
        return (
            <div className="top-ui-bar" id="top-bar">
                <div className="center">
                    <div className="menu-toggle" onClick={this.toggleMenu}>
                        <div className="stripe"> </div>
                        <div className="stripe"> </div>
                        <div className="stripe"> </div>
                    </div>
                    <span className="welcome-ui-box"> Welkom, Bob Varwyk </span>
                </div>
                <div className="overlay" id="overlay" onClick={this.toggleMenu}> </div>
                <Menu history={this.props.history} menuHandler={this.toggleMenu}/>
            </div>
        )
    }
}

export default MenuBar;