import React from 'react';
import Header from '../General/Header';
import Axios from 'axios';

class AllergiesComponent extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        const user = JSON.parse(window.localStorage.getItem("User"));
        this.state = {
            userId: user.id,
            allergies: ["Dairy", "Peanut", "Soy", "Egg", "Seafood", "Sulfite", "Gluten", "Sesame", "Tree nut", "Grain", "Shellfish", "Wheat"],
            active : [],
            inactive : []
        }
        this.handleChange = this.handleChange.bind(this);
        this.getAllergies = this.getAllergies.bind(this);
    }
    
    componentDidMount() {
        this._isMounted = true;
        Axios.get(`http://localhost:8080/allergies/${this.state.userId}`)
            .then(response => {
                const activeAllergies = response.data.message;
                const inactiveAllergies = this.state.allergies.filter((element) => !activeAllergies.includes(element));
                if(this._isMounted) {
                    this.setState({
                        inactive: inactiveAllergies,
                        active: activeAllergies
                    })
                }
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleChange(event) {
        const { value } = event.target;
        console.log(value)
        Axios.put(`http://localhost:8080/account/allergies/${this.state.userId}`, {
            allergyName: value
        }).then(response => {
            const activeAllergies = response.data.message;
            const inactiveAllergies = this.state.allergies.filter((element) => !activeAllergies.includes(element));
            this.setState({
                inactive: inactiveAllergies,
                active: activeAllergies
            })
        })
    }

    getAllergies(arrayName) {
        const checkBoxes = [];
        let checkedState = arrayName === "active" ? true : false;
        for(var allergy in this.state[arrayName]) {
            var type = this.state[arrayName][allergy];
            checkBoxes.push(
                <li key={type}> 
                    {type}
                    <label key={type} className="switch">
                        <input onChange={this.handleChange} value={type} type="checkbox" checked={checkedState}/>
                        <span className="slider round"></span>
                    </label>
                </li>
            )
        }
        return checkBoxes;
    }
    
    render() {
        if(this.props.showScreen !== "allergies") {
            return null;
        }
        
        // Init empty Toggles array
        let Toggles = [];
        // Get the array with active allergies
        let activeAllergies = this.getAllergies("active");
        // Get the array with inactive allergies
        let inactiveAllergies = this.getAllergies("inactive");
        // Merge the two arrays to one Toggles array with shows the checkboxes on the screen.
        Toggles = activeAllergies.concat(inactiveAllergies);
         
        return (
            <div className="component-container" id="allergies">
               <Header title="Uw allergieen" />
               <div className="center"> 
                    <p> Op deze pagina kunt u allergieen op actief en inactief zetten op uw account. Deze allergieen zullen in de eerst volgende week recepten worden meegenomen en hier zal rekening mee worden gehouden. </p>
                    <ul> 
                        {Toggles}
                    </ul>
                </div>
            </div>
        );
    }
}

export default AllergiesComponent;