import React from 'react';
import {Snackbar} from '@material/react-snackbar';
import Axios from 'axios';
import { AnimateOnChange } from 'react-animation';

class GroceryCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: JSON.parse(localStorage.getItem("User")).id,
            day: "",
            groceries: [],
            apiResponse: []
        }
        this.handleGroceryChange = this.handleGroceryChange.bind(this);
        this.closePopUp = this.closePopUp.bind(this);
    }

    componentDidMount() {
        let day = this.props.day
        let groceries = this.props.groceries

        if(day !== "" && groceries.length > 0) {
            this.setState({
                day: day,
                groceries: groceries
            })
        }
    }

    handleGroceryChange(event) {
        const {value} = event.target;
        Axios.put(`http://localhost:8080/groceries/${this.state.userId}/update/${this.state.day}`, {
            groceryItem: value
        }).then(response => {
            this.setState({
                apiResponse: [response.data.message, response.data.succes]
            })
        })
    }

    closePopUp() {
        this.setState({
            apiResponse: []
        })
    }

    render() {

        const groceries = [];
        this.state.groceries.forEach(element => {
            groceries.push(
                <li key={element}> 
                    <label htmlFor={element} className="grocery-item"> {element} </label>
                    <input onChange={this.handleGroceryChange} type="checkbox" id={element} name={element} value={element} /> 
                    <span className="checkmark"> <i className="fa fa-check"></i> </span>
                </li>
            )
        });

        return (
            <div className="grocery-card">
                <div className="index-bar"> <h3> {this.props.day} </h3> <button className="info"> <i className="fas fa-info"></i> </button> </div>
                {groceries}
                <AnimateOnChange>
                    {this.state.apiResponse.length > 0 ? (         
                        this.state.apiResponse[1] ? (
                            <Snackbar className="succes" onClose={this.closePopUp} timeoutMs="10000"  message={this.state.apiResponse[0]} actionText={<i className="fal fa-times"></i>} />
                        ) : (
                            <Snackbar className="failed" onClose={this.closePopUp} timeoutMs="10000"  message={this.state.apiResponse[0]} actionText={<i className="fal fa-times"></i>} />            
                        )
                    ) : (
                        null
                    )}
                </AnimateOnChange>
            </div>
        )
    }
}

export default GroceryCard;