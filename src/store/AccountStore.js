import { createStore } from 'redux';
import accountReducer from '../reducers/AccountReducer';

export default createStore(accountReducer);