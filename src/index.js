import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Route, MemoryRouter, BrowserRouter} from "react-router-dom";
import StartScreen from './components/account/StartScreen';
import AppInterface from './components/App/AppInterface';
import recipeView from './components/App/Recipe Components/RecipeDetails Components/RecipeView';
import GenerateSchedule from './components/App/Recipe Components/Generate Components/GenerateSchedule';


let routing = "";

if(window.localStorage.getItem("User") === null) {
    routing = (
        <BrowserRouter>
                <Route path="/" exact component={StartScreen}/>
                <Route path="/:action" exact component={StartScreen} />
                <Route path="/:action/:token" exact component={StartScreen} />
                <Route path="/password/reset/:resettoken" exact component={StartScreen} />
        </BrowserRouter>
    )
}

if(window.localStorage.getItem("User") !== null) {
    routing = (
        <BrowserRouter>
                <Route path="/" exact component={AppInterface}/>
                <Route path="/generate/recipes" exact component={GenerateSchedule}/>
                <Route path="/:component" exact component={AppInterface} />
                <Route path="/recipe/:recipeId" exact component={recipeView} />
        </BrowserRouter>
    )
}

ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
