// Function to return current day name for filtermenu component
export function getCurrentDayName() {
    const days = ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"]
    let day = new Date();
    return days[day.getDay()];
}