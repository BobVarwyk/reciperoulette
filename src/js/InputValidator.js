export function validateFormFields({firstName, lastName, email, password, passwordConfirm}) {
    // Email regex for valid email
    var validEmailFormat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // Password regex for one uppercase, one lowercase and one number and between 8 and 40 characters
    var validPasswordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,40}$/;
    var errors = {
        firstName:
            !firstName || firstName.trim().length === 0
            ? "Uw voornaam is verplicht"
            : null,
        lastName:
            !lastName || lastName.trim().length === 0
            ? "Uw achternaam is verplicht"
            : null,
        email:
            !email || email.trim().length === 0 || !validEmailFormat.test(email)
            ? "Uw email is niet correct."
            : null,
        password:
            !password || password.trim().length === 0 || !validPasswordFormat.test(password)
            ? "Uw wachtwoord moet minimaal een hoofdletter, cijfer en speciaal teken bevatten."
            : null,  
        passwordConfirm:
            !passwordConfirm || passwordConfirm !== password
            ? "De wachtwoorden komen niet overeen"
            : null
        }
        return errors;
}